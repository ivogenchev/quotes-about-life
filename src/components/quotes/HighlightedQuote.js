import classes from "./HighlightedQuote.module.css";

const HighlightedQuote = (props) => {
  return (
    <figure className={classes.quote}>
      <p>&#8220; {props.text} &#8221;</p>
      <figcaption>- {props.author}</figcaption>
    </figure>
  );
};

export default HighlightedQuote;
